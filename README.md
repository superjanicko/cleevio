# cleevio TEST v01

## design
- mobilefirst \
- figma: https://www.figma.com/file/AjBO4INpSLHbxaOMxGIvCS/Cleevio---FE

## devstack
react, typescript, webpack, bootstrap

## git lab
add to gitlab repository

## rest API
static json: https://gist.githubusercontent.com/davidzadrazil/43378abbaa2f1145ef50000eccf81a85/raw/d734d8877c2aa9e1e8c1c59bcb7ec98d7f8d8459/countries.json

## development
- [ ] layout - html
- [x] components - html
- [ ] css - layout + components
- [x] menu - html
- [ ] menu - css
- [ ] login - layout
- [ ] login - context/redirect

## presentation
- gitlab - public code
- application deployment - cleevio.kakao.name
- rest proxy?

## pripomienky ku dizajnu
- slaby kontrast (napr: textu na tlacitkach - seda-seda, zlta hviezdicka na zltom tlacitku je tragicka)
- na ikony by som radsej pouzil nejaku knihovnu napr fontawesome
- "api" je divne, array a v nom 1 objekt....divny response
