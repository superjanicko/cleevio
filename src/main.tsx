import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {
  LoginPage,
  ContentPage,
  Header,
  AuthProvider
} from './';
import 'styles/style.scss';
import './favicon-32x32.png';
import {HashRouter as Router, Route, Switch} from 'react-router-dom';

const renderApp = () => {
  ReactDOM.render((
    <Router>
      <AuthProvider>
      <Switch>
        <Route path={'/content'} component={ContentPage} />
        <Route component={LoginPage} />
      </Switch>
      </AuthProvider>
    </Router>
  ), document.getElementById('app'));
};

renderApp();
