export interface TripInterface {
  status: TripStatus,
  country: Countries,
  flag: string;
  address?: string[];
  date?: {
    start: string;
    end: string;
  }
  details?: string[];
  modified?: string;
  created?: string;
}

export interface Country {
  value: string;
  text: string;
  icon: string;
}

export enum Countries {
  germany = 'Germany',
  austria = 'Austria',
  france = 'France',
  portugal = 'Portugal',
  czechia = 'Czech republic',
  sweden = 'Sweden',
  uk = 'United Kingdom',
  greece = 'Greece',
}

export enum TripStatus {
  current = 'Current Trip',
  planned = 'Planned',
  drafts = 'Drafts',
  favorites = 'Favorites',
  recent = 'Recent',
}

// icons

export enum IconColor {
  gray,
  blue,
  red,
  white,
  yellow
}
export enum IconSize {
  small = 10,
  regular= 20,
  big = 40,
}
export enum IconType {
  standard,
  flag,
}

export enum InputType {
  'text',
  'hidden',
  'password',
  'radio',
  'checkbox',
}