import * as React from "react";
import {Button, Input} from "../components";
import {useEffect, useState} from "react";
import {InputType} from "../models";
import {useHistory} from "react-router";

export const LoginPage = () => {
  let history = useHistory();

  const [username, setUsername] = useState<string>(undefined);
  const [usernameError, setUsernameError] = useState<string>(undefined);
  const [usernameIsNotValid, setUsernameIsNotValid] = useState<boolean>(undefined);

  const [password, setPassword] = useState<string>(undefined);
  const [passwordError, setPasswordError] = useState<string>(undefined);
  const [passwordIsNotValid, setPasswordIsNotValid] = useState<boolean>(undefined);

  const [formValid, setFormValid] = useState<boolean>(undefined);

  const handleSubmit = () => {
    if (username) {
      setUsernameIsNotValid(false);
      setUsernameError(undefined);
    } else {
      setUsernameIsNotValid(true);
      setUsernameError('No username!');
    }

    if (password) {
      setPasswordIsNotValid(false);
      setPasswordError(undefined);
    } else {
      setPasswordIsNotValid(true);
      setPasswordError('No password!');
    }
  }

  useEffect(() => {
    if (usernameIsNotValid === false && passwordIsNotValid === false) {
      history.push('/content');
    }
  }, [usernameIsNotValid, passwordIsNotValid])

  const handleChange = ({target: {name, value}}) => {
    switch(name) {
      case 'username':
        setUsername(value);
        setUsernameError(undefined);
        setUsernameIsNotValid(undefined);
        break;
      case 'password':
        setPassword(value);
        setPasswordError(undefined)
        setPasswordIsNotValid(undefined)
        break;
    }
  }

  return (<div className="login">
    <h1 className="login__heading">Login to Test Project</h1>
    <form className="login__form">
      <fieldset className="fieldset">
        <label className="label" htmlFor="username">User name</label>
        <Input
          onChange={handleChange}
          name="username"
          isNotValid={usernameIsNotValid}
          block
          id="username"
          placeholder={'User name'}
          error={usernameError}
        />
      </fieldset>
      <fieldset className="fieldset">
        <label className="label" htmlFor="password">Password</label>
        <Input
          onChange={handleChange}
          name="password"
          isNotValid={passwordIsNotValid}
          block
          id="password"
          type={InputType.password}
          placeholder={'Password'}
          error={passwordError}
        />
      </fieldset>
      <Button
        title={'Login'}
        className={'button--blue'}
        onClick={() => handleSubmit()}
      />
    </form>

    <ul className="login__buttons">
      <li><Button small plain title={'Forgot password?'} /></li>
      <li><Button small plain title={'Register'} /></li>
    </ul>
  </div>);
};
