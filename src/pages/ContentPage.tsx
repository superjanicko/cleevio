import * as React from 'react';
import {CountrySelector, TripSet, Header} from '../components';
import {Countries, Country, TripInterface, TripStatus} from '../models';
import {useState} from "react";

export interface ApiReponse{
  label: string;
  data: Country[];
}

const data: TripInterface[] = [
  {
    status: TripStatus.current,
    country: Countries.austria,
    flag: 'austria',
    address: ['22, Wöllach, 9562, Wöllach,', 'Feldkirchen, Kärnten'],
    date: {
      start: 'Jul 14',
      end: 'Sep 20, 2019'
    },
    details: ['Password required'],
  }, {
    status: TripStatus.planned,
    country: Countries.uk,
    flag: 'uk',
    address: ['58 The Avenue', 'SOUTH EAST LONDON SE313QI'],
    date: {
      start: 'Jul 14',
      end: 'Sep 20, 2019'
    },
    details: ['Password required'],
  }, {
    status: TripStatus.drafts,
    country: Countries.germany,
    flag: 'germany',
    address: ['165, L547, 37586, Dassel,', 'Northeim, Niedersachsen '],
    date: {
      start: 'Dec 17',
      end: 'Dec 23, 2019'
    },
    modified: '3 days ago',
  }, {
    status: TripStatus.favorites,
    country: Countries.czechia,
    flag: 'czechia',
    address: ['107, Na Ležánkách, 530 03,', 'Pardubice, Pardubický kraj'],
  }, {
    status: TripStatus.recent,
    country: Countries.france,
    flag: 'france',
    address: ['Rue aux Moines, 70120, Lavigney,', 'Haute-Saône, Bourgogne-Franche'],
    date: {
      start: 'Jul 14',
      end: 'Sep 20, 2019'
    },
    created: 'Tatiana Kucharova',
  }, {
    status: TripStatus.recent,
    country: Countries.portugal,
    flag: 'portugal',
    address: ['5, Unnamed Road, 5000, Vila Real'],
    date: {
      start: 'Dec 17',
      end: 'Dec 23, 2019'
    },
  },
]

const SearchData: ApiReponse = {
  "label":"Results",
  "data":[
    {
      "value":"germany",
      "text":"Germany",
      "icon":"flag-germany"
    }, {
      "value":"austria",
      "text":"Austria",
      "icon":"flag-austria"
    }, {
      "value":"france",
      "text":"France",
      "icon":"flag-france"
    }, {
      "value":"portugal",
      "text":"Portugal",
      "icon":"flag-portugal"
    }, {
      "value":"czechia",
      "text":"Czech Republic",
      "icon":"flag-czechia"
    }, {
      "value":"sweden",
      "text":"Sweden",
      "icon":"flag-sweden"
    }, {
      "value":"uk",
      "text":"United Kingdom",
      "icon":"flag-uk"
    }, {
      "value":"greece",
      "text":"Greece",
      "icon":"flag-greece"
    },
  ]
};

export const ContentPage = () => {
  const [devel] = useState(false);
  return (<>
      <div className="content">
      {!devel && <><div className="content__side">
        <h2 className="content__h2 desktop-only">Trips</h2>
        <TripSet
          trips={data.filter((set: TripInterface) => {
            return set.status === TripStatus.current
          })}
          status={TripStatus.current}
        />
        <TripSet
          trips={data.filter((set: TripInterface) => {
            return set.status === TripStatus.planned
          })}
          status={TripStatus.planned}
        />
      </div>
      <div className="content__top">
        <h1 className="content__heading">Home</h1>
        <div className="content__search">
          <h2>Start New Trip</h2>

          <CountrySelector
            countries={SearchData.data}
          />
        </div>
      </div>
      <div className="content__mid">
        <TripSet
            trips={data.filter((set: TripInterface) => {
              return set.status === TripStatus.drafts
            })}
            status={TripStatus.drafts}
        />
        <TripSet
            trips={data.filter((set: TripInterface) => {
              return set.status === TripStatus.favorites
            })}
            status={TripStatus.favorites}
        />
        <TripSet
            trips={data.filter((set: TripInterface) => {
              return set.status === TripStatus.recent
            })}
            status={TripStatus.recent}
        />
      </div></>}
    </div>
    <Header />
  </>);
};
