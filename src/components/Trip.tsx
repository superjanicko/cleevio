import * as React from 'react';
import {Countries, TripInterface, TripStatus} from '../models';
import {Button, Icon} from './';

interface Props {
  data: TripInterface
}

export const Trip = ({data}: Props) => {
  return (<div className="trip">
    <div className="trip__main">
      <div className="trip__title">
        <h4>{data.country}</h4><Icon flag={data.flag} />
      </div>
      <div className="trip__content">
        <table className="table-form">
          <tbody>
            {data.address && <tr className={'table-form__set'}>
              <th className="table-form__label mobile-only">Address</th>
              <td className="table-form__value">
                <ul>
                  {data.address.map((line: string, index: number) => {
                    return (<li key={index}>{line}</li>);
                  })}
                </ul>
              </td>
            </tr>}
            {data.date && <tr className={'table-form__set'}>
              <th className="table-form__label mobile-only">Date</th>
              <td className="table-form__value">
                {data.date.start}
                {data.date.end? <> - {data.date.end}</>: ''}
              </td>
            </tr>}
            {data.modified && <tr className={'table-form__set'}>
              <th className="table-form__label mobile-only">Modified</th>
              <td className="table-form__value">{data.modified}</td>
            </tr>}
            {data.details && <tr className={'table-form__set'}>
              <th className="table-form__label mobile-only">Details</th>
              <td className="table-form__value">
                <ul>
                {data.details.map((detail: string, index: number) => {
                  return (<li key={index}>{detail}</li>);
                })}
                </ul>
              </td>
            </tr>}
            {data.created && <tr className={'table-form__set'}>
              <th className="table-form__label  mobile-only">Created by</th>
              <td className="table-form__value">
                {data.created}
              </td>
            </tr>}
          </tbody>
        </table>
      </div>
    </div>
    <div className="trip__buttons">
      {(data.status === TripStatus.current || data.status === TripStatus.planned) &&
        <Button title={'View details'} after><Icon icon={'arrow'} /></Button>}
      {data.status === TripStatus.drafts && <>
        <Button title={'Complete'} after> <Icon icon={'arrow'} /></Button>
        <Button title={'Remove'} color={'red'} only><Icon icon={'recycle'} /></Button>
      </>}
      {data.status === TripStatus.favorites && <>
        <Button title={'Repeat'} after><Icon icon={'repeat'} /></Button>
        <Button title={'Remove'} color={'yellow'} only><Icon icon={'star'} /></Button>
      </>}
      {data.status === TripStatus.recent && <>
        <Button title={'Detail '} after><Icon icon={'arrow'} /></Button>
        <Button title={'Add to favourite'} only><Icon icon={'star'} /></Button>
        <Button title={'Repeat'} only><Icon icon={'repeat'} /></Button>
      </>}
    </div>
  </div>);
}
