import * as React from 'react';
import {useEffect} from 'react';
import {IconColor} from '../models';

interface Props {
  color?: IconColor,
  flag?: string,
  icon?: string,
  after?: boolean,
}

export const Icon = ({color, flag, icon, after}: Props) => {
  let classes: string = 'icon';
  classes += (flag)? ` icon--flags icon--flag-${flag}`: ` icon--icons icon--i-${icon}`;
  classes += after? ' icon--after': '';
  return (<i
    className={classes}
  />);
}
