import * as React from 'react';
import {Button} from './Button';
import {Icon} from './Icon';
import {useState} from 'react';

export const Header = () => {
  const [openMenu, setOpenMenu] = useState(false);

  return (<header className="header">
    <nav className={`header__nav${openMenu? ' header__nav--is-active': ''}`}>
      <Button title={'New Trip'} className={'button--blue'}>
        <Icon icon={'plus'} after={true} />
      </Button>
      <ul className='margin-top-2'>
        <li><Button title={'Home'} plain active>
          <Icon icon={'home'} />
        </Button></li>
        <li><Button title={'Favourites'} plain>
          <Icon icon={'star'} />
        </Button></li>
        <li><Button title={'Planned'} plain>
          <Icon icon={'watches'} />
        </Button></li>
        <li><Button title={'Drafts'} plain>
          <Icon icon={'edit'} />
        </Button></li>
        <li><Button title={'History'} plain>
          <Icon icon={'planet'} />
        </Button></li>
      </ul>
    </nav>
    <div className="mobile-only header__buttons">
      <Button
        only
        inline
        title={'Menu'}
        onClick={() => {
          setOpenMenu(!openMenu)
        }}
        className={openMenu? 'button--is-active': ''}
      >
        <Icon icon={'hamburger'} />
      </Button>
      <Button
        only
        inline
        title={'Settings'}
        href={'/'}
      >
        <Icon icon={'settings'} />
      </Button>
    </div>
  </header>);
};
