import * as React from 'react';
import {useEffect, useState} from 'react';
import {Input} from './Input';
import {Button} from './Button';
import {Icon} from './Icon';
import {ApiReponse} from '../pages';
import {Country} from '../models';

interface ListItemProps {
  data: Country;
  defaultCheck: boolean;
  onChange?: any;
  part: string;
}

interface Props {
  countries: Country[];
}

export const CountrySelector = ({countries}: Props) => {
  const [query, setQuery] = useState<string>(undefined);
  const [filtered, setFiltered] = useState<Country[]>(undefined);
  const [selected, setSelected] = useState<Country[]>([]);

  const onSelectedHandler = (name: string, remove?: boolean) => {
    remove?
      setSelected(selected.filter((country: Country)=> {
        return country.value !== name
      })):
      countries.map((country: Country) => {
        (country.value === name) &&
        !(selected.indexOf(country) !== -1) &&
        setSelected([
          ...selected,
          country
        ]);
      });
  }
  const onResetHandler = () => {
    setQuery(undefined);
    setFiltered(countries);
  }

  const onChangeHandler = (e) => {
    const searchString: string = (e.target.value).toLowerCase();
    setQuery(searchString);
    setFiltered(countries.filter((country: Country)=> {
      return (country.text).toLowerCase().includes(searchString);
    }))
  }

  return (<div className={`searchSelect${(query && filtered.length > 0)? ' searchSelect--active': ''}`}>
    <div className="searchSelect__container">
      <Input
        reset={'searchSelect__reset'}
        className={`searchSelect__input${(query && filtered.length > 0)? ' input--select': ''}`}
        onChange={onChangeHandler}
        onReset={onResetHandler}
        block
        placeholder="Where do you want to travel?"
      />
      <ul className="searchSelect__list">{(query && filtered.length > 0) &&
        filtered.map(({value, text}: Country, index: number) => {
          return <li className="searchSelect__item" key={index}>
            <Button plain onClick={() => onSelectedHandler(value)} title={text}>
              <Icon flag={value}/>
            </Button>
          </li>;
      })}</ul>
    </div>

    {(selected && selected.length > 0) && <ul className="listSelected">{selected.map(({text, value}: Country, index: number) => {
      return <li className="listSelected__item" key={index}>
        <Button className="button--inline" onClick={() => onSelectedHandler(value, true)} title={text}>
          <Icon flag={value}/>
        </Button>
      </li>
    })}</ul>}
  </div>);
}
