import * as React from 'react';
import {createContext, useState} from 'react';

export interface contextAuth {
  isLoggedIn: boolean;
  setIsLoggedIn: any;
}

export const AuthContext = createContext<Partial<contextAuth>>({});
export const AuthProvider = ({children}) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  return (<AuthContext.Provider value={{
      isLoggedIn: isLoggedIn,
      setIsLoggedIn: setIsLoggedIn,
    }}>
    {children}
  </AuthContext.Provider>);
};
