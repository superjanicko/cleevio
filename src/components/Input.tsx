import * as React from 'react';
import {useEffect, useState, useRef} from 'react';
import {Button} from './Button';
import {Icon} from './Icon';
import {InputType} from "../models";

interface Props {
  defaultValue?: string;
  type?: InputType;
  label?: string;
  placeholder?: string;
  block?: boolean;
  reset?: string;

  id?: string;
  name?: string;

  onChange?: any;
  onReset?: any
  className?: string;
  isNotValid?: boolean;
  error?: string;
}

export const Input = ({name, error, isNotValid, id, reset, defaultValue, className, block, type, placeholder, onChange, onReset}: Props) => {
  const textInput = useRef<HTMLInputElement>();
  const [val, setVal] = useState(undefined);
  const onResetHandler = () => {
    textInput.current.value = '';
    onReset();
    setVal(undefined);
  };
  const onChangeHandler = (e) => {
    if(onChange) {
      onChange(e);
      setVal(e.target.value)
    }
  }

  let classes: string = 'input';
  classes += className? ` ${className}`: '';
  classes += block? ' input--block': '';
  classes += isNotValid === undefined?
    '':
    (isNotValid?
      ' input--is-not-valid':
      ' input--is-valid'
    );

  return (<>{!!reset?
    <div className={reset}>
      <input
        name={name}
        id={id}
        ref={textInput}
        onChange={(e) => onChangeHandler(e)}
        className={classes}
        type={InputType[type] || 'text'}
        placeholder={placeholder}
      />
      {error && <small className={'error'}>{error}</small>}
      {val && <Button
        onClick={onResetHandler}
        className={'input__reset'}
        title={'reset'}
        plain
        only
      >
        <Icon icon={'reset'} />
      </Button>}
    </div>:
    <>
      <input
        name={name}
        id={id}
        ref={textInput}
        onChange={(e) => onChangeHandler(e)}
        className={classes}
        type={InputType[type] || 'text'}
        placeholder={placeholder}
      />
      {error && <small className={'error'}>{error}</small>}
    </>}
  </>);
}
