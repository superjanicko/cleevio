import * as React from 'react';
import {Countries, TripInterface, TripStatus} from '../models';
import {Trip} from './Trip';
import {Button} from './Button';

interface Props {
  status: TripStatus;
  title?: string;
  trips: any;
}

export const TripSet = ({title, status, trips}: Props) => {
  return (<>{trips.length > 0 && <section className="trip-set">
    <div className="trip-set__header">
      <h3 className="trip-set__title">{title || status}</h3><Button inline small title={'Show all'} />
    </div>
    <div className="trip-set__content">
      {trips.map((trip: TripInterface, index: number) => {
        return (<Trip key={index} data={trip}/>)
      })}
    </div>
  </section>}
</>);
}
