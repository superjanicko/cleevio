import * as React from 'react';

interface Props {
  children?: any;
  color?: string;

  title: string;

  bgColor?: string;
  txtColor?: string;
  after?: boolean;
  only?: boolean;
  className?: string;

  plain?: boolean;
  active?: boolean;
  small?: boolean;
  inline?: boolean;

  onClick?: any;
  href?: string;
}

export const Button = ({href, inline, onClick, small, active, after, only, title, plain, className, children, color, bgColor, txtColor}: Props) => {
  let classes: string = 'button';
  classes += className? ` ${className}`: '';
  classes += plain? ` button--plain`: '';
  classes += inline? ` button--inline`: '';
  classes += small? ` button--small`: '';
  classes += active? ` button--is-active`: '';
  classes += color? ` button--${color}`: '';
  classes += bgColor? ` button--bg-${bgColor}`: '';
  classes += txtColor? ` button--txt-${txtColor}`: '';
  classes += only? ' button--icon-only': '';
  return (<a className={classes} title={title} onClick={onClick} href={href}>
    {!after && children && <span className="button__icon">{children}</span>}
    {!only && <span className="button__title">{title}</span>}
    {after && children && <span className="button__icon">{children}</span>}
  </a>);
};
