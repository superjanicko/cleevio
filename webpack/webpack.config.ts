import * as path from 'path';
import * as _ from 'lodash';
import * as webpack from 'webpack';
import * as HtmlWebpackPlugin from 'html-webpack-plugin';
import * as MiniCssExtractPlugin from 'mini-css-extract-plugin';
import * as TerserJsPlugin from 'terser-webpack-plugin';
import * as OptimizeCSSAssetsPlugin from 'optimize-css-assets-webpack-plugin';
import {CheckerPlugin} from 'awesome-typescript-loader';
import {scripts} from './scripts';
import {getOutputName, isProduction, resolveWebSrcPath, root} from './util';

const rootDir = (__dirname.substr(__dirname.length-7) === 'webpack') ? path.resolve(__dirname, '../../') : path.resolve(__dirname, '../');
let aliases = _.mapValues(scripts.aliases, function (scriptPath) {
  return path.resolve(rootDir + scriptPath);
});

module.exports = function () {
  const config: any = {};
  config.entry = {
    app: resolveWebSrcPath('main.tsx'),
  };
  config.output = {
    publicPath: '/',
    path: root('web'),
    filename: getOutputName('[name].js'),
    chunkFilename: getOutputName('[name].[hash].bundle.js'),
  };

  config.resolve = {
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js', '.css', '.less', '.html'],
    alias: aliases,
    modules: [path.resolve(rootDir, "src"), "node_modules"],
  };

  config.devtool = 'eval-cheap-module-source-map';
  // ie11 config.devtool = false;
  config.mode = 'development';
  config.performance = { hints: false };
  console.log('isProduction(): ', isProduction())
  if (isProduction()) {
    config.output.publicPath = `/`;
    config.devtool = false;
    config.mode = 'production';
    config.optimization = {
      runtimeChunk: {
        name: 'config'
      },
      splitChunks: {
        cacheGroups: {
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendor',
            chunks: 'all'
          }
        }
      },
      usedExports: true,
      minimizer: [
        new TerserJsPlugin({
          terserOptions: {
            parallel: 4,
            compress: {
              drop_console: true
            }
          }
        }),
        new OptimizeCSSAssetsPlugin(),
      ]
    };
  }

  config.module = {
    rules: [
      {
        test: /\.js$/,
        include: /node_modules\/(format-duration)/,
        use: [{
          loader: 'babel-loader',
          options: {
            presets: 'es2015'
          }
        }],
      }, {
        test: /\.tsx?$/,
        loaders: ['awesome-typescript-loader'],
        exclude: /node_modules/
      }, {
        test: /\.html$/,
        loader: 'raw-loader',
        exclude: [root('src/index.html')]
      }, {
        test: /favicon-32x32.png$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '/favicon-32x32.png'
          }
        }],
      }, {
        test: /\.png|\.jpe?g|\.gif|\.svg|\.woff|\.woff2|\.ttf|\.eot|\.svg$/,
        exclude: /favicon-32x32.png$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: 'assets/[name].[hash].[ext]'
          }
        }],
      }, {
        test: /\.(s*)css$/,
        use: [
          // 'style-loader',
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader"
        ]
      },
    ]
  };

  config.plugins = [
    new webpack.DefinePlugin({
      PRODUCTION: JSON.stringify(isProduction()),
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      chunksSortMode: 'auto',
      favicon: 'src/favicon-32x32.png',
    }),
    new MiniCssExtractPlugin({
      filename: "[name].[hash].css",
      chunkFilename: "[name].[hash].css"
    }),
    new CheckerPlugin(), // awesome-typescript-loader
  ];

  console.log('isProduction()',isProduction());

  if (!isProduction()) {
    config.devServer = {
      disableHostCheck: true,
      port: 3000,
      https: true,
      contentBase: './<%= pkg.web %>/',
      historyApiFallback: true,
      watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
        clientLogLevel: 'info',
      },
      hot: true,
      // inline: true,
      stats: {
        // colors: true,
        errors: true,
      },
      proxy: {
        "/api": {
          pathRewrite: {'^/api':''},
          target: `https://gist.githubusercontent.com/davidzadrazil/43378abbaa2f1145ef50000eccf81a85/raw/d734d8877c2aa9e1e8c1c59bcb7ec98d7f8d8459/countries.json`,
          changeOrigin: true,
          secure: false,
          logLevel: 'debug',
        },
      }
    };

    config.plugins.push(new webpack.HotModuleReplacementPlugin());
  }
  return config;
}();
