import * as path from 'path';

// Helper functions
export const ROOT = path.resolve(__dirname, '..');

export const getWebContentBase = ():string => path.join(__dirname, '../src/');
export const getWebSrcPath = (): string => path.resolve('src');
export const getDistDirectory = (): string => path.resolve('target');
export const isProduction = (): boolean => process.env.NODE_ENV === 'production';
export const resolveWebSrcPath = (file:string): string => './src/' + file;
export const getDevEnv = ():string => process.env.DEV_ENV || 'PROD';
export const getApi = ():string => process.env.API || 'http://localhost:3001';
export const getOutputName = (name:string): string => isProduction()? name: name.replace('[hash].', '');
export const root = (...args):string => path.join.apply(path, [ROOT].concat(Array.prototype.slice.call(args, 0)));
