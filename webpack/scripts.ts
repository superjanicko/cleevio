// const publicPath = isProduction()?'/public':'/../public';
// const nodeModulesPath = isProduction()?'/node_modules':'/../node_modules';
const publicPath = '/public';
const nodeModulesPath = '/node_modules';

export const scripts: any = {
    aliases: {
        "images": publicPath+"/img",
        // "font-awesome": nodeModulesPath+"/font-awesome/css/font-awesome.min.css",

        // "bootstrap": nodeModulesPath+"/bootstrap/dist/css/bootstrap.css",
    },
};
